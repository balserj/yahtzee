# Yahtzee

A modified game of Yahtzee that allows the player to roll 5 dice and
choose a scoring category. Points are awarded at the end of each round based
on what category was chosen. The player goes through rounds until all scoring categories
are used, at which point the game ends and their final score is displayed. The main
program consists of four main classes; Die, Dice, ScoreCard and Game. The class Die
rolls a 6-sided die and the class Dice utilizes Die to roll 5 dice at the same time.
ScoreCard keeps track of the 13 categories the user may choose depending on their dice
rolls. The categories will be displayed as well as tell the user if a specific category
has been used or not and keep track of the user's score. The Game class is responsible for
the play and gameOver functions of the game. Once all categories have been used up by the
user, the game will end.